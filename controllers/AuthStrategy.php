<?php

use woauth_controllers_SNDriver as SNDriver;

class woauth_controllers_AuthStrategy{

    private static $contextAuth;

    function __construct(SNDriver $context)
    {
        $this->contextAuth = $context;
    }

    public function getToken($code){
        return $this->contextAuth->getToken($code);
    }

    public function getUserInfo(){
        return $this->contextAuth->getUserInfo();
    }

    public function getRefAuth(){
        return $this->contextAuth->getRefAuth();
    }

    public function Auth($code){
        $this->contextAuth->getToken($code);

        return $this->contextAuth->getUserInfo();
    }

    public function getUserEmail(){
        return $this->contextAuth->getUserEmail();
    }

    public function getPhoto(){
        return $this->contextAuth->getPhotoUser();
    }

    public function getAllInfo(){
        return $this->contextAuth->showAllUserInfo();
    }

}
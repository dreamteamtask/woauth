<?php

class woauth_controllers_VKOAuth implements woauth_controllers_SNDriver{

    // OAuth Client seting
    protected $AuthUrl = 'http://oauth.vk.com/authorize';
    protected $client_id = '';
    protected $client_secret = '';
    protected $redirect_uri = '';
    protected $token = '';
    // get fields
    protected $clients_fields = [
        'uid',
        'sex',
        'city',
        'bdate',
        'email',
        'country',
        'timezone',
        'photo_50',
        'photo_100',
        'photo_200',
        'last_name',
        'photo_max',
        'photo_big',
        'first_name',
        'screen_name',
        'photo_200_orig',
        'photo_400_orig',
        'photo_max_orig'
    ];
    // user info
    protected $user_screen_name = '';
    protected $user_FirstName = '';
    protected $user_photo_big = '';
    protected $user_LastName = '';
    protected $user_allInfo = [];
    protected $user_photos = [];
    protected $user_bdate = '';
    protected $user_email = '';
    protected $user_sex = '';
    protected $user_id = '';
    // send message
    protected $distributionList = [];
    protected $distributionListString = '';
    protected $message = '';
    protected $lat = '';
    protected $long = '';

    public function __construct($client, $redirect, $secret)
    {
        $this->client_id = $client;
        $this->redirect_uri = $redirect;
        $this->client_secret = $secret;
    }

    protected function getClientsFields(){
        return implode(',',$this->clients_fields);
    }

    protected function getValueFromArray($array,$key){
        if(isset($array) && isset($array[$key])){
            return $array[$key];
        }else{
            return '';
        }
    }

    public function getToken($code)
    {
        $params = array(
            'client_id' => $this->client_id,
            'client_secret' => $this->client_secret,
            'code' => $code,
            'redirect_uri' => $this->redirect_uri,
        );
        $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);
        if(isset($token['email'])){
            $this->user_email = $token['email'];
        }
        if(isset($token['user_id'])){
            $this->user_id = $token['user_id'];
        }
        $this->token = $token;
        return $token;
    }

    public function recipientsCreation($idSocialRecipients){
        if(gettype($idSocialRecipients) == 'array'){
            $this->distributionList = $idSocialRecipients;
        }else{
            array_push($this->distributionList,$idSocialRecipients);
        }
        $this->distributionListString = implode(',',$this->distributionList);
        return $this;
    }

    public function addLocation($lat,$long){
        if(isset($lat) && isset($long) && $lat != '' && $long != ''){
            $this->lat = $lat;
            $this->long = $long;
            return $this;
        }
        return false;
    }

    public function sendMessages($message,$idRecipient){
        if(isset($this->token['access_token'])){
            $this->message = $message;
            $params = [
                'user_id' => $idRecipient,
                'user_ids' => $this->distributionListString,
                'message' => $this->message,
                'lat' => $this->lat,
                'long' => $this->long
            ];
            $messageInfo = json_decode(file_get_contents('https://api.vk.com/method/messages.send' . '?' . urldecode(http_build_query($params))), true);
            return $messageInfo;
        }
    }

    public function getUserInfo()
    {
        if (isset($this->token['access_token'])) {
            $userInfo = [];
            $params = array(
                'uids'         => $this->token['user_id'],
                'fields'       => $this->getClientsFields(),
                'access_token' => $this->token['access_token']
            );
            $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
            if (isset($userInfo['response'][0]['uid'])) {
                $userInfo = $userInfo['response'][0];
                $this->user_allInfo = $userInfo;
                $userInfo['snid'] = $userInfo['response'][0]['uid'];
                $this->user_allInfo['email'] = $this->user_email;
            }
            // filling the user information
            $this->user_bdate = $this->getValueFromArray($userInfo,'bdate');
            $this->user_FirstName = $this->getValueFromArray($userInfo,'first_name');
            $this->user_id = $this->getValueFromArray($userInfo,'uid');
            $this->user_sex = $this->getValueFromArray($userInfo,'sex');
            $this->user_photos['photo_50'] = $this->getValueFromArray($userInfo,'photo_50');
            $this->user_photos['photo_100'] = $this->getValueFromArray($userInfo,'photo_100');
            $this->user_photos['photo_200'] = $this->getValueFromArray($userInfo,'photo_200');
            $this->user_photos['photo_max'] = $this->getValueFromArray($userInfo,'photo_max');
            $this->user_photos['photo_big'] = $this->getValueFromArray($userInfo,'photo_big');
            $this->user_photos['photo_200_orig'] = $this->getValueFromArray($userInfo,'photo_200_orig');
            $this->user_photos['photo_max_orig'] = $this->getValueFromArray($userInfo,'photo_max_orig');
            return $this->user_allInfo;
        }else{
            return false;
        }
    }

    public function getPhotoUser(){
        return $this->user_photos;
    }

    public function setClientID($clientId)
    {
        $this->client_id = $clientId;
    }

    public function getUserEmail(){
        return $this->user_email;
    }

    public function showAllUserInfo()
    {
        return $this->user_allInfo;
    }

    public function setRedirectUrl($redirect)
    {
        $this->redirect_uri = $redirect;
    }

    public function setSecret($secret)
    {
        $this->client_secret = $secret;
    }

    public function getRefAuth()
    {
        $params = [
            'client_id'     => $this->client_id,
            'redirect_uri'  => $this->redirect_uri,
            'response_type' => 'code',
            'display' => 'popup',
            'scope' => 'offline,email,photos'
        ];
      return $this->AuthUrl . '?' . urldecode(http_build_query($params));
    }

    public function getAuthUrl()
    {
        return $this->AuthUrl;
    }

}
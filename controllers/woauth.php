<?php

use woauth_controllers_AuthStrategy as AuthStrategy;
use woauth_controllers_VKOAuth as VKOAuth;
use woauth_controllers_FBOAuth as FBOAuth;
use woauth_controllers_googleOAuth as GoOAuth;
use woauth_controllers_linkedOAuth as linkedOAuth;
use wpyramide_controllers_frontent_auth as BasicAuth;

class woauth_controllers_woauth extends common_controller
{
    private $oDriver = NULL;

    public function __construct($parser)
    {
        parent::__construct($parser);
    }

    public function index($pars = '')
    {
        $this->vklink = $this->getVk();
        $this->fblink = $this->getFb();
        $this->golink = $this->getGoogle();
        $this->linlink = $this->getLinked();
        $this->render();
    }

    public function getFb($pars = '')
    {
        $sClientId = '298983577158900';
        $sRedirectURL = 'http://po6.starvex.com.ua/ru/fblogin';
        $sKey = 'e9235681697ca02c642865b6307f8530';

        $oFBOAuth = new FBOAuth($sClientId, $sRedirectURL, $sKey);
        $this->oDriver = new AuthStrategy($oFBOAuth);

        $sCode = Request::get('code');

        if (!empty($sCode)) {
            $aUserInfo = $this->oDriver->Auth($sCode);

            $BasicAuth = new BasicAuth();

            if(!empty($aUserInfo)){
                $BasicAuth->checkOAuth($aUserInfo);
                header("Location: ".Routing::url('/profile')); //на страницу ввода email
                exit();
            }else{
                header("Location: ".Routing::url('/authpage')); //на страницу ввода email
                exit();
            }

        }

        return $this->oDriver->getRefAuth();
    }

    public function getVk()
    {

        $sClientId = '5634655';
        $sRedirectURL = 'http://po6.starvex.com.ua/ru/vklogin';
        $sKey = 'RQSyBIhOzTfv1ahBTGb2';

        $oVKOAuth = new VKOAuth($sClientId, $sRedirectURL, $sKey);

        $this->oDriver = new AuthStrategy($oVKOAuth);

        $sCode = Request::get('code');

        if (!empty($sCode)) {
            $aUserInfo = $this->oDriver->Auth($sCode);

            $BasicAuth = new BasicAuth();

            if(!empty($aUserInfo)){
                $BasicAuth->checkOAuth($aUserInfo);
                header("Location: ".Routing::url('/profile')); //на страницу ввода email
                exit();
            }else{
                header("Location: ".Routing::url('/authpage')); //на страницу ввода email
                exit();
            }

        }
        return $this->oDriver->getRefAuth();
    }

    public function getGoogle()
    {

        $sClientId = '252063677631-mo4hdk9cfpnageh9hcolv2u6nuf4f6pg.apps.googleusercontent.com';
        $sRedirectURL = 'http://po6.starvex.com.ua/ru/googlelogin';
        $sKey = 'E5vorFdM0r1phkzRqN60O4RQ';

        $GoOAuth = new GoOAuth($sClientId, $sRedirectURL, $sKey);

        $this->oDriver = new AuthStrategy($GoOAuth);

        $sCode = Request::get('code');

        if (!empty($sCode)) {
            $aUserInfo = $this->oDriver->Auth($sCode);

            $BasicAuth = new BasicAuth();
            if(!empty($aUserInfo)){
                $BasicAuth->checkOAuth($aUserInfo);
                header("Location: ".Routing::url('/profile')); //на страницу ввода email
                exit();
            }else{
                header("Location: ".Routing::url('/authpage')); //на страницу ввода email
                exit();
            }

        }

        return $this->oDriver->getRefAuth();
    }

    public function getLinked()
    {

        $sClientId = '7794szwzgrw6x9';
        $sRedirectURL = 'http://po6.starvex.com.ua/ru/linkedlogin';
        $sKey = 'K8ScJvCc6Ylj5MzL';

        $linkedOAuth = new linkedOAuth($sClientId, $sRedirectURL, $sKey);

        $this->oDriver = new AuthStrategy($linkedOAuth);

        $sCode = Request::get('code');

        if (!empty($sCode)) {
            $aUserInfo = $this->oDriver->Auth($sCode);

            $BasicAuth = new BasicAuth();

            if(!empty($aUserInfo)){
                $BasicAuth->checkOAuth($aUserInfo);
                header("Location: ".Routing::url('/profile')); //на страницу ввода email
                exit();
            }else{
                header("Location: ".Routing::url('/authpage')); //на страницу ввода email
                exit();
            }

        }
        return $this->oDriver->getRefAuth();
    }

}
<?php

class woauth_controllers_FBOAuth implements woauth_controllers_SNDriver{
    protected $AuthUrl = 'https://www.facebook.com/dialog/oauth';
    protected $sTokenUrl = 'https://graph.facebook.com/oauth/access_token';
    protected $client_id = '1';
    protected $client_secret = '';
    protected $redirect_uri = '';
    protected $token = '';
    
    public function __construct($client,$redirect,$secret){
        $this->client_id = $client;
        $this->redirect_uri = $redirect;
        $this->client_secret = $secret;
    }

    public function getAuthUrl()
    {
        // TODO: Implement getAuthUrl() method.
    }
    
    public function getToken($code)
    {
        $params = array(
            'client_id'     => $this->client_id,
            'client_secret' => $this->client_secret,
            'redirect_uri'  => $this->redirect_uri,
            'code'          => $code
        );
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->sTokenUrl . '?' . urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_HTTPGET, 1);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);
        //$tokenInfo = json_decode($result);
        foreach (explode('&', $result) as $aItem){
            list($key, $value) = explode('=', $aItem);
            $aTmp[$key] = $value;
        }
        $this->token = $aTmp;
        return $aTmp;
    }
    
    public function getRefAuth()
    {
        $params = array(
                    'redirect_uri'  => $this->redirect_uri,
                    'response_type' => 'code',
                    'client_id'     => $this->client_id
                );
        return $this->AuthUrl . '?' . urldecode(http_build_query($params));
    }

    public function getUserInfo()
    {
        if (isset($this->token['access_token'])) {
            $params['access_token'] = $this->token['access_token'];
            $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?fields=id,email,first_name,last_name,gender,picture&debug=all&' . urldecode(http_build_query($params))), true);
            if(!empty($userInfo['email'])){
                return false;
            }else{

                $userInfo['snid'] = $userInfo['id'];
                $this->user_allInfo = $userInfo;
            }
            return $userInfo;
        }else{
            return false;
        }
    }
    
    public function getPhotoUser()
    {
        return $this->user_photo_big;
    }
    
    public function getUserEmail(){
        return $this->user_email;
    }

    public function setRedirectUrl($redirect)
    {
        $this->redirect_uri = $redirect;
    }

    public function setClientID($clientId)
    {
        $this->client_id = $clientId;
    }

    public function setSecret($secret)
    {
        $this->client_secret = $secret;
    }

    public function showAllUserInfo()
    {
        return $this->user_allInfo;
    }
}
<?php

class woauth_controllers_googleOAuth implements woauth_controllers_SNDriver{

    protected $AuthUrl = 'https://accounts.google.com/o/oauth2/auth';
    protected $client_id = '';
    protected $client_secret = '';
    protected $redirect_uri = '';
    protected $token = '';
    // user info
    protected $user_screen_name = '';
    protected $user_FirstName = '';
    protected $user_photo_big = '';
    protected $user_LastName = '';
    protected $user_allInfo = [];
    protected $user_photos = [];
    protected $user_bdate = '';
    protected $user_email = '';
    protected $user_sex = '';
    protected $user_id = '';


    public function __construct($client, $redirect, $secret)
    {
        $this->client_id = $client;
        $this->redirect_uri = $redirect;
        $this->client_secret = $secret;
    }

    public function getAuthUrl()
    {
        // TODO: Implement getAuthUrl() method.
    }

    protected function getValueFromArray($array,$key){
        if(isset($array) && isset($array[$key])){
            return $array[$key];
        }else{
            return '';
        }
    }

    public function getRefAuth()
    {
        $params = array(
            'redirect_uri'  => $this->redirect_uri,
            'response_type' => 'code',
            'client_id'     => $this->client_id,
            'scope'         => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'
        );
        return $this->AuthUrl . '?' . urldecode(http_build_query($params));
    }

    public function getUserInfo()
    {
        if (isset($this->token['access_token'])) {
            $params['access_token'] = $this->token['access_token'];
            $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
            if (isset($userInfo['id'])) {
                $userInfo['snid'] = $userInfo['id'];
                $this->user_allInfo = $userInfo;
            }
            $this->user_email = $this->getValueFromArray($userInfo,'email');
            $this->user_id = $this->getValueFromArray($userInfo,'id');
            $this->user_FirstName = $this->getValueFromArray($userInfo,'given_name');
            $this->user_LastName = $this->getValueFromArray($userInfo,'family_name');
            $this->user_sex = $this->getValueFromArray($userInfo,'gender');
            $this->user_photo_big = $this->getValueFromArray($userInfo,'picture');
            return $userInfo;
        }else{
            return false;
        }
    }

    public function getPhotoUser()
    {
        return $this->user_photo_big;
    }

    public function getToken($code)
    {
        $params = array(
            'client_id'     => $this->client_id,
            'client_secret' => $this->client_secret,
            'redirect_uri'  => $this->redirect_uri,
            'grant_type'    => 'authorization_code',
            'code'          => $code
        );
        $url = 'https://accounts.google.com/o/oauth2/token';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($curl);
        curl_close($curl);
        $tokenInfo = json_decode($result, true);
        $this->token = $tokenInfo;
        return $tokenInfo;
    }

    public function getUserEmail(){
        return $this->user_email;
    }

    public function setRedirectUrl($redirect)
    {
        $this->redirect_uri = $redirect;
    }

    public function setClientID($clientId)
    {
        $this->client_id = $clientId;
    }

    public function setSecret($secret)
    {
        $this->client_secret = $secret;
    }

    public function showAllUserInfo()
    {
        return $this->user_allInfo;
    }

}
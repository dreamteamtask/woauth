<?php

interface woauth_controllers_SNDriver{

    public function __construct($client,$redirect,$secret);
    public function setClientID($clientId);
    public function getToken($code);
    public function getUserInfo();
    public function showAllUserInfo();
    public function setSecret($secret);
    public function setRedirectUrl($redirect);
    public function getAuthUrl();
    public function getRefAuth();
    public function getPhotoUser();
    public function getUserEmail();
}